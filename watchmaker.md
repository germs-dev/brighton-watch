# Watchmaker resources

"Do better if possible and that is always possible.": François-Constantin to Jacques-Barthélémi Vacheron

## Books and films

- Practical Watch Repairing by Donald de Carlo: essential reading (get this first)
- [The Watchmaker's Apprentice](https://www.imdb.com/title/tt2958390/) featuring George Daniels and Roger W. Smith (and watch this to whet your appetite)
- [Teddy Baldassarre interviewing Max Büsser](https://www.youtube.com/watch?v=700nTdwv-k0)
- Lessons in Horology by Grossman
- Joseph Bulova School of Watchmakers
- Practical Treatise on Gearing by Brown and Sharpe (I haven't read this but what a title!)
- [Masters of Time: The World of Swiss Complicated Watches](https://www.youtube.com/watch?v=CtkcLjiNy_0): the work of independent watchmakers Pilippe Dufour and Antonio Prezioso

## YouTube channels

- [Wristwatch Revival](https://www.youtube.com/c/wristwatchrevival): Marshall is a bit of a legend, essential viewing for aspiring watchmakers
- [Nekkid Watchmaker](https://youtube.com/@NekkidWatchmaker): less polished production but a bit of a dude

## Modkits

- [Namoki Mods](https://www.namokimods.com/): Seiko 5 mods
- [DIY Watch Club](https://shop.diywatch.club/)
- [Watch Modz](https://watch-modz.com/product-category/cases/)
- [Crystal Times](https://usa.crystaltimes.net/) (US delivery only)
- [Seiko Mods](https://www.seikomods.com/) (Crystal Times sister site)
- [Secondhand Mods](https://secondhandmods.com/)
- [DLW Watches](https://www.dlwwatches.com/)
- [ModMode](https://modmodewatches.com/)
- eBay: search for a movement, e.g., "nh35a" or ["6497"](https://www.ebay.co.uk/sch/i.html?_nkw=6497)
- [Almond Mods](https://almondmods.com/)

## Beat frequencies

| Beats per second | Vibrations per hour |
|---|---|
| 3   | 10800 |
| 4   | 14400 |
| 5   | 18000 |
| 5.5 | 19800 |
| 6   | 21600 |
| 7   | 25200 |
| 8   | 28800 |

## Clean

At this point it can start to become quite expensive: not only for the machines but also the consumables that you probably can't sell on quite so easily. Be careful putting your pallet forks and balance wheel in solvent as it can dissolve the [shellac](https://en.wikipedia.org/wiki/Shellac) that holds the pallet and roller jewels in place.

- Lighter fluid: readily available from a newsagent for £2.99
- [Renata Essence](https://www.cousinsuk.com/product/renata-essence) balance spring degreaser £11.50 (standard)
- [Basket 25mm Mini Watch Part Cleaning Basket Economy](https://www.hswalsh.com/product/basket-25mm-mini-watch-part-cleaning-basket-economy-hb125)
: get a few of these, you can spend quite a lot but I went economy and I don't know any better; but I prefer the little brass threaded ones
- Screw top jam jar, recycled and small, the shallower the better to get your bits out and not use lots of cleaning fluid

You can do a job with the above, however, ultrasonic cleaners like [this one](https://www.amazon.co.uk/DK-SONIC-Household-Ultrasonic-Eyeglasses/dp/B08S6V52MV/) are a great way in. Not as efficient as a proper four-stage cleaning machine but very reasonably-priced and entertaining to use. You can also impressive your friends and relatives by cleaning jewellery and spectacles.

## Rebuild and lubricate

Lubrication can become a very complicated business, so let's look at the key areas of interest. As a bare minimum you will need:

- A light oil for the quickest moving parts ([Moebius Syntalube 9010](https://www.cousinsuk.com/product/moebius-9010-syntalube)): outrageously expensive!
- A grease for the high-friction metal-on-metal components ([Moebius 8300](https://www.cousinsuk.com/product/moebius-8300-all-purpose-favourite))
- Oiler, fine (Bergeon red) for the jewels
- Oiler, medium (Bergeon green) for the grease; go for the cheap oilers to start with

I realise that suggesting using only two viscosities will cause some people to spit out their tea/beer, but _you have to start somewhere_. Also, Donald de Carlo only talks about "watch oil" and "clock oil" in the above watchmaking bible, so there you go.

__Full disclosure__: I also dip my oilers straight into the jars, which is probably illegal in Switzerland.

### Various received wisdoms about oil

"If you need to do it cheaply you can just use 8000 and DX, as it's a non synthetic it'll just mean the service intervals will be shorter. It’s not ideal because Moebius 8000 is a natural oil so that’s means velocity is not as high as synthetic oil."

- Moebius 9010: fast wheels
- Moebius D5: slow wheels; e.g., arbor, cannon pinion, hour wheel, minute wheel
- Moebius 9415: pallet fork jewels
- Moebius 9501, DX: keyless works
- Moebius 8217: "braking grease" for inside barrel wall (getting serious!)

## Timegraphers

A Timegrapher is the go-to tool for the job and at the low end they are surprisingly inexpensive. However, you can get some useful results from a phone app: use with an earbud mic if you're not in a completely quiet environment. You may find for some movements they give you very poor results when regulating, which could lead you to think the movement is poorly when it's actually in good health.

## Mainspring winders

You can fix a lot by replacing parts rather than making new ones. But cleaning and refitting mainsprings is probably your first step into really getting series. Winding a mainspring back up in the barrel _can_ be done by hand, however, people will get very cross about it if they find out, and you may be compromising the power reserve of the watch. A proper winder is the answer but a set is fiendishly expensive.

## Stem cutting

There are dedicated tools for this but you can get away with a pair of side cutters and some sandpaper; I like to finish on a Bergeon polishing buff. Now, how much stem should you leave? Often it seems quite subjective but 0.1mm between crown and case is a good rule of thumb. You could measure it precisely with a feeler gauge, but equally, if you have a micrometer, you can find a piece of paper the correct thickness and use that. In reality, the tolerances in the watch may be larger than 0.1mm so you shouldn't worry about it too much.

## Case clamps

Something that cost me a lot of time and frustration is flicking a case clamp deep into a movement. So when casing up I've made it a habit of tipping the movement up a little -- i.e., not flat against the case cushion -- so if the clamp or screw decides to jump it will probably end up on your desk and not beneath the balance spring.

## Tweezers

You can spend quite a bit on wood-tipped tweezers, and while they do give you a little more ping protection and safety handling dials, you're still going to need metal ones for fine work. So I think they're a complementary addition to your toolbox rather then essential (certainly for a beginner.)

## Pro tools and supplies

The top two offer some good economy options if you can't justify £25 on a tiny cleaning basket, for instance.

- [H&S Walsh](https://www.hswalsh.com/)
- [Cousins](https://www.cousinsuk.com)
- [Gleave London](https://gleave.london/straps/)
- [Eternal tools](https://www.eternaltools.com/)
- [Lucius Atelier](https://luciusatelier.com/)
- [DIY Watch Club](https://shop.diywatch.club/collections/watchmaking-tools)
- [Welwyn](https://welwynwatchparts.co.uk/)
