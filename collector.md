# Collector resources

## Micro brands

- [Christopher Ward](https://www.christopherward.com/)
- [Farer](https://farer.com/)
- [Giuliano Mazzuoli](https://giulianomazzuoli.com/)
- [BA111OD](https://ba111od.com/)
- [Kopf](http://kopf.watch/)
- [Minhoon Yoo](https://minhoonyoo.com/)
- [The Camden Watch Company](https://www.camdenwatchcompany.com/)
- [Molnar Fabry](Molnar Fabry): based in Slovakia
- [Bourlinger](https://www.bourlinger.com/)
- [CAP](https://en.cap-watch.com/): sustainability, recycled materials
- [Sequent](https://sequentworld.com/en-gb/products/elektron-hr-2-2-space-white-rubber): mechanical smartwatch
- [Baltany](https://baltany.co.uk/): inexpensive, some nice vintage styling
- [Extropian](https://www.extropian.co/watches)

## In-house movements

- [A. Lange & Söhne](https://www.alange-soehne.com/)
- [F.P.Journe](https://www.fpjourne.com/)
- [Speake-Marin](https://www.speake-marin.com/)
- [Frederic Jouvenot](https://fjouvenot.com/solar-deity-collection/amaterasu-titanium/): unusual sundial face
- [Moritz Grossman](https://en.grossmann-uhren.com/)
- [Leica](https://leica-camera.com/en-GB/World-of-Leica/Leica-Watch): camera-inspired
- [Glashütte Original](https://www.glashuette-original.com/)

## Alien technology

- [HYT](https://hytwatches.com/)

## Miyota

- [Sternglas](https://www.sternglas.com/): that Glashütte look on a budget
- [Vario](https://vario.sg/pages/ww1-trench-watch): inspired by WW1 trench watches
- [Autogromo](https://autodromo.com/): driving-inspired

## Seagull

- [Studio Underd0g](https://underd0g.com/)
- [Forge and Foster](https://forgeandfoster.com/): Hublot kind of aesthetic for £300

## NH35

- [Shoreham](https://www.shorehamwatches.com/)
- [Spinnaker](https://spinnaker-watches.com/)
- [Adley](https://watchadley.co/): bike-inspired design
- [Camden Watch Company](https://www.camdenwatchcompany.com/)

## Watches you should have bought

But are sold out now so tough.

- [Christopher Ward Bel Canto](https://www.christopherward.com/c1-bel-canto-blue.html)
- [Studio Underd0g Go0fy Panda (Gen2)](https://underd0g.com/products/01gpb)

## Straps

### Low- to mid-priced

- [Gleave London](https://gleave.london/straps/)
- [Barton](https://www.bartonwatchbands.com/) (also Horween leather)
- [Welwyn](https://welwynwatchparts.co.uk/collections/leather-straps)

### Mid-priced

- [Crafter Blue](https://www.crafterblue.com/)
- [Strap Bandits](https://www.strapbandits.com/) (fabric straps)
- [Wrist Buddy](https://wristbuddys.com/)
- [Uncle](https://unclestraps.com/)

### Mid- to high-priced

- [Strap Tailor](https://thestraptailor.com/)
- [Delug](https://delugs.com/)
- [Monstraps](https://monstraps.com/)
- [Watch Gecko](https://www.watchgecko.com/)
- [Hirsch](https://www.hirschstraps.com/) (I've got a couple of these)

### High-priced

- [Forstner](https://forstnerbands.com/): steel bracelets
- [Vulcain](https://www.vulcanwatchstraps.com/): Rolex rubber straps
- [Rubber B](https://rubberb.com/): rubber straps
- [Everest](https://www.everestbands.com/)
- [Café Leather](https://cafeleather.com/best-leather-watch-straps/): for all your leather requirements
- [Jean Rousseau](https://www.jean-rousseau.com/gb/product-category/watch-straps-gb/)

## Stores

- [Watchfinder](https://www.watchfinder.co.uk)
- [Teddy Baldasssarre](https://teddybaldassarre.com)
- [Pride and Pinion](https://prideandpinion.com)
- [Delray Watch](https://delraywatch.com)
- [Watches of Switzerland](https://www.watches-of-switzerland.co.uk/)
- [The Timeteller shop](https://thetimetellershop.com/)

## Price trackers

- [Chrono24](https://www.chrono24.co.uk/)
- [Watch Charts](https://watchcharts.com/)

## Enthusiast cleaning

- [Nanoclear](https://nanoclear.com/)
- [Polywatch](https://www.polywatch.de/) (usually for vintage acrylic crystals)

## Enthusiast books

- [A Man and His Watch](https://www.waterstones.com/book/a-man-and-his-watch/matthew-hranek/9781579657147) (Paul Newman's Daytona) by Matthew Hranek

## Enthusiast tools

Also see the pro tools section below.

- [Everest](https://www.everestbands.com/collections/watch-tools-springbars)
- [Barton](https://www.bartonwatchbands.com/en-gb/collections/watch-band-hardware)

## Getting on the Rolex waiting list

See [this article](https://www.vulcanwatchstraps.com/blog/how-to-beat-the-rolex-waiting-list).

- Meet with multiple dealers
- Some value purchase history, some value relationships
- Set up appointments and multiple ADs and assess which store is best
- Work your way up
- Only buy what you love, don't fall for dealer tricks
- Grow the relationship, be the person they want to call

## Setting the date safely

Move the hands to 6 o'clock, change the date to the day _before_ today, and then move the time forward to get the correct half of the day. Never fiddle with the date between 9pm and 4am; _but_, if it has a [phantom date](https://calibercorner.com/phantom-date/) then good luck!
