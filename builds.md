# Builds

What's on the bench at the moment?

## Raketa 2609.HA 11.5''' (listed)

Solid Soviet-era watch, 38mm, just needs a little love. See [Ranfft](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&Raketa_2609_HA).

- [x] Estimate amplitude (slow-mo video): approx 200&deg;
- [x] Potential loose dial (rattles when you tap it but case screws are tight): was missing a dial screw
- [x] Clean dial
- [x] Put case in ultrasonic
- [x] Polywatch crystal

## Omega 131.019 (steel)

> So I rehomed this cal 601 and serviced it... but it doesn't run for very long. Could well be the mainspring as it was the first one I've done by hand.

22M serial (1965), cal 601 non-runner, arrived in a case for an automatic, has some homemade case clamps.

- [x] Buy correct NOS case for movement
- [x] New mainspring  
- [x] Order new 30.8mm crystal
- [x] Fit new crystal
- [x] Ultrasonic clean of keyless works
- [x] Trial fit brand new seconds hand (could be too shiny compared to old hands) -- looks fine
- [x] Stem doesn't engage properly, decase and investigate -- it's the wrong crown for the case!
- [x] Try correct stem/crown -- winding very stiff, hmmm
- [x] Trial with correct stem outside of the case -- doesn't fit
- [x] Order new case clamps -- see [Omega case clamps](https://www.cousinsuk.com/PDF/categories/8427_Omega%20Case%20Clamps.pdf)
- [x] Order new stem and crown (from Cousins) -- stem engages nicely
- [x] Trim stem and fit crown
- [x] Try oiling the jewels, only achieving approx. 170&deg; amplitude

### Stems

- [Omega](https://gleave.london/stem-omega-480-1106/)
- [Generic](https://www.cousinsuk.com/sku/details/omega-generic-movement-parts/ome6001106g)

## Omega 161.009 gold-plated

This was all ready to sell but has developed a rattle when you shake it.

Really nice dress watch, if a little small. Serial 22190717 (1965), cal 552,
crown very worn, can _just_ see the Omega logo, stem not engaging, rotor a
little worn.

- [x] Service (external)
- [x] New leather strap
- [ ] Get the back off and see what the rattle is

## 1975 Nino AS 2066 11.5'''

Low amplitude, needs a service. See [Ranfft](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&&2uswk&AS_2066).

- [ ] Clean dial
- [ ] Ultrasonic case
- [ ] Service

## Cauny small dress watch with sub-seconds

Non-runner, balance OK, stem very difficult to remove, rolled gold bracelet, broken mainspring, movement [FHF 63](http://www.ranfft.de/cgi-bin/bidfun-db.cgi?10&ranfft&0&2uswk&FHF_63).

- [x] Remove from case -- pop the front
- [x] Clean dial -- didn't make much difference
- [x] Check if it runs by turning barrel
- [x] Order donor movement
- [ ] Replace mainspring (swap from donor)
- [ ] Service

## Citizen 2800A (sold)

Time drifts, quite erratic 180&deg; amplitude.

- [ ] Review keyless works
- [ ] Service
- [ ] Replace main spring (gets very low amplitude on a low wind)
- [ ] Bezel has a gap
- [ ] Grease gasket

## Longines 12.92 12'''

Hands a little fragile, no case, 27.8mm movement, 26mm dial. I found a 16-jewel
version with a wobbly balance to use as a donor, so basically I'll make one
good one out of the two. Might've been easier to just to stick the good balance
in the 16-jewel, but it seems both balances are a little wobbly. Will service
and try again.

See [Longines serial numbers](https://millenarywatches.com/longines-serial-number/).

- https://www.barnebys.co.uk/blog/wwi-100-years-on-watches-from-the-trenches
- https://www.google.com/search?q=longines+ww1+trench+watch
- [Image search](https://www.google.com/imgres?imgurl=https%3A%2F%2Fimages.prismic.io%2Fbarnebys%2F01d6a1a5f39dfc6857dd460f7755d6b28bd1be08_longines-heritage-military-original-1918-trench-watch.jpg%3Fw%3D900%26auto%3Dformat%252Ccompress%26cs%3Dtinysrgb&imgrefurl=https%3A%2F%2Fwww.barnebys.co.uk%2Fblog%2Fwwi-100-years-on-watches-from-the-trenches&tbnid=lw_elLi2nAtsJM&vet=12ahUKEwjy9JzSzML8AhU-U6QEHQzABQEQMygFegUIARD7Ag..i&docid=RwYiaDfqZKk3LM&w=900&h=882&q=longines%20ww1%20trench%20watch&client=ubuntu&ved=2ahUKEwjy9JzSzML8AhU-U6QEHQzABQEQMygFegUIARD7Ag)
- https://64.media.tumblr.com/a4e51fb324cd1155f1c9ab553ac5c9d2/tumblr_nrkv81FsEE1tin58jo1_1280.jpg

Probably should be in a [gold
case](https://www.vintage-wristwatches.co.uk/watches-catalogue/archived-watches/longines-watch-archive/1930-9ct-gold-cushion-vintage-longines-watch-retailed-by-j-w-benson-4359/). But on the lookout for any usable pin-set case.

### 15-jewel

Serial 4929243 (1930).

- [x] Clean dial
- [x] Remove broken stem
- [x] Fit missing ratchet wheel -- took wheel and barrel (with mainspring) from the other one
- [ ] Service
- [ ] Refit good balance

### 16-jewel

Serial 5107778 (1932), broken balance but otherwise movement is OK. Slightly later but has the hour wheel jewel.

- [x] Clean dial: not very effective
- [ ] Find new case

## 1916 Borgel trench watch 13'''

Marvin 362, 29.5mm; bezel &#x2300; 30.2mm. Bezel measured thrice: 30.17, 30.21, 30.24. So I went for a 30.4mm crystal and it was a success.

- [x] Order 30.4mm crystal
- [x] Polish silver
- [x] Replace crystal
- [x] Order donor movement for practice/spares
- [ ] Rachet wheel is missing a tooth, still works but might replace from donor for completeness
- [x] Pusher has come loose during cleaning -- fits in OK anyway
- [ ] Service
- [ ] Repair original strap -- see [leather conditioner](https://www.watchobsession.co.uk/blogs/watchobsession-blog/how-to-care-for-leather-watch-strap)
- [ ] Grease threads on case
- [ ] Trial a new rubber strap with the metal insert

## Fortis FHF ST-96 11.5'''

Might just sell this for spares.

- [ ] Order new crystal -- opening measures 30.96mm
- [ ] Review missing case screws
- [ ] Measure timing
